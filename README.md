# ClearLinux Insync Install Script

- Download the Fedora .rpm from official site: [https://www.insynchq.com/downloads](https://www.insynchq.com/downloads)
- Download `clearlinux-insync-install.sh` in the same folder
- Open your terminal in that same folder and run: `chmod +x clearlinux-insync-install.sh`
- Then run: `./clearlinux-insync-install.sh`
