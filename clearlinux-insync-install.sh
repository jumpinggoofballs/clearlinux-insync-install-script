#! /bin/bash
# Download Fedora rpm from official site and run this in the same folder

file-roller -h insync*
cd usr/
echo "#!/bin/bash
cd /usr/local/lib64/insync
LC_TIME=C exec ./insync \"\$@\" --ca-path /etc/ssl/certs/ " > bin/insync
sudo cp bin/insync /usr/local/bin/
sudo cp -r lib/insync /usr/local/lib64/
sudo cp -r share/* /usr/local/share/
sudo cp -r share/mime/packages/insync-helper.xml /usr/local/share/mime/packages/
sudo mv /usr/local/lib64/insync/libX11.so.6 /usr/local/lib64/insync/libX11.so.6.bak
cd ..
rm -r usr/
